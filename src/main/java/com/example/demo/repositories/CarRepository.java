package com.example.demo.repositories;


import com.example.demo.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    @Query("select car from Car car left join car.engine engine where " +
            "car.brand is not null or " +
            "engine.gears = 5")
    List<Car> getCars();

    @Query("select car from Car car where " +
            "car.brand is not null or " +
            "car.engine.gears = 5")
    List<Car> getCarsNoJoin();
}
