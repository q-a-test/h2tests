package com.example.demo.repositories;

import com.example.demo.entities.Car;
import com.example.demo.entities.Engine;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CarRepositoryH2Test {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private EngineRepository engineRepository;

    @Test
    void save() {
        Engine engine = engineRepository.save(Engine.builder().build());
        Car car = carRepository.save(Car.builder().engine(engine).build());
        assertNotNull(car.getId());
    }

    @Test
    void getCars() {
        Engine engine = engineRepository.save(Engine.builder().gears(5L).build());
        carRepository.save(Car.builder().engine(engine).build());
        carRepository.save(Car.builder().brand("Ford").build());

        List<Car> all = carRepository.findAll();
        assertEquals(2, all.size());
        List<Car> cars = carRepository.getCars();
        assertEquals(2, cars.size());
        List<Car> carsNoJoin = carRepository.getCarsNoJoin();
        assertEquals(1, carsNoJoin.size());
    }

}
